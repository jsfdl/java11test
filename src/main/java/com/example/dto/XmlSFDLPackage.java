package com.example.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.Collections;
import java.util.List;

public class XmlSFDLPackage {

    @XmlElement(name = "Packagename")
    private String packagename;

    @XmlElement(name = "BulkFolderMode")
    private boolean bulkFolderMode;

    @XmlElementWrapper(name = "FileList")
    @XmlElement(name = "FileInfo")
    private List<XmlFileInfo> xmlFileInfo;

    @XmlElementWrapper(name = "BulkFolderList")
    @XmlElement(name = "BulkFolder")
    private List<BulkFolder> bulkFolderList;

    public String getPackagename() {
        return packagename;
    }

    public boolean isBulkFolderMode() {
        return bulkFolderMode;
    }

    public List<BulkFolder> getBulkFolderList() {
        return Collections.unmodifiableList(bulkFolderList);
    }

    public List<XmlFileInfo> getXmlFileInfo() {
        return xmlFileInfo;
    }

}
