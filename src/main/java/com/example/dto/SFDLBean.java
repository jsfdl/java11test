package com.example.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * DTO for XML marshalling/unmarshalling
 */
@XmlRootElement(name = "SFDLFile")
public class SFDLBean {

    @XmlElement(name = "Description")
    private String description;

    @XmlElement(name = "Uploader")
    private String uploader;

    @XmlElement(name = "SFDLFileVersion")
    private Integer sfdlFileVersion;

    @XmlElement(name = "Encrypted")
    private boolean encrypted;

    @XmlElement(name = "ConnectionInfo")
    private XmlConnectionInfo xmlConnectionInfo;

    @XmlElement(name = "Packages")
    private XmlPackages xmlPackages;

    @XmlElement(name = "MaxDownloadThreads")
    private int maxDownloadThreads;

    public String getDescription() {
        return description;
    }

    public String getUploader() {
        return uploader;
    }

    public Integer getSfdlFileVersion() {
        return sfdlFileVersion;
    }

    public boolean isEncrypted() {
        return encrypted;
    }

    public XmlConnectionInfo getXmlConnectionInfo() {
        return xmlConnectionInfo;
    }

    public XmlPackages getXmlPackages() {
        return xmlPackages;
    }

    public int getMaxDownloadThreads() {
        return maxDownloadThreads;
    }

}
