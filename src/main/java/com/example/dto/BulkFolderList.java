package com.example.dto;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

class BulkFolderList {

    @XmlElement(name = "BulkFolderList")
    private List<BulkFolder> bulkFolderList;

    public List<BulkFolder> getBulkFolderList() {
        return bulkFolderList;
    }
}
